package fallback

import (
	"testing"
)

func TestFallback_string(t *testing.T) {
	// the value to be returned here should be 2.
	switch Fallback("", "value 1", "value 2") {
	case "":
		t.Error("default value returned")
	case "value 2":
		t.Error("value returned was not the first non-default value")
	}
}

func TestFallback_int(t *testing.T) {
	// the value to be returned here should be 1.
	switch Fallback(0, 1, 2) {
	case 0:
		t.Error("default value returned")
	case 2:
		t.Error("value returned was not the first non-default value")
	}
}

func TestFallback_bool(t *testing.T) {
	switch Fallback(false, true, false) {
	case false:
		t.Error("default value returned")
	}
}

func TestFallback_float64(t *testing.T) {
	// the value to be returned here should be 1.0.
	switch Fallback(0.0, 1.0, 2.0) {
	case 0.0:
		t.Error("default value returned")
	case 2.0:
		t.Error("value returned was not the first non-default value")
	}
}

func TestFallback_struct(t *testing.T) {
	type test struct {
		String  string
		Int     int
		Float64 float64
	}

	var value0, value1, value2 test
	value1 = test{"value 1", 1, 1.0}
	value2 = test{"value 2", 2, 2.0}

	switch Fallback(value0, value1, value2) {
	case value0:
		t.Error("default value returned")
	case value2:
		t.Error("value returned was not the first non-default value")
	}
}

func TestFallback_ptr(t *testing.T) {
	type test struct {
		String  string
		Int     int
		Float64 float64
	}

	var value0, value1, value2 *test
	value1 = &test{"value 1", 1, 1.0}
	value2 = &test{"value 2", 2, 2.0}

	switch Fallback(value0, value1, value2) {
	case nil:
		t.Error("default value returned")
	case value2:
		t.Error("value returned was not the first non-default value")
	}
}
