package fallback

// Fallback returns the first non-empty/non-default/non-zero value if available, otherwise returns the default value of T.
func Fallback[T comparable](values ...T) (t T) {
	for _, value := range values {
		if value != t {
			return value
		}
	}

	return t
}
